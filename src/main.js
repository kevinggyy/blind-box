import { createApp } from 'vue'
import App from './App.vue'
import './index.css'

// const app = createApp(App)
// app.use(VueDapp, {
//     infuraId: '...', // optional: for enabling WalletConnect and/or WalletLink
//     appName: '...', // optional: for enabling WalletLink
//     appUrl: '...', // optional: for enabling MetaMask deep link for mobile devices
// })
// app.mount('#app')
createApp(App).mount('#app')
