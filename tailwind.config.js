module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      keyframes: {
        'ease-scale': {
          '0%': { opacity: 0, backgroundSize: '350%' },
          '25%': { opacity: '1' },
          '50%': { opacity: '1' },
          '75%': { opacity: '0' },
          'to': { opacity: 0, backgroundSize: '280%' },
        }
      },
      animation: {
        'ease-scale': 'ease-scale 10s linear infinite'
      }
    },
  },
  plugins: [],
}